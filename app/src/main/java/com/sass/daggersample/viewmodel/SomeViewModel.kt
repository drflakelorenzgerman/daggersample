package com.sass.daggersample.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class SomeViewModel @Inject constructor(): ViewModel() {
    fun doSth()
    {
        Log.d("Dagger2Sample","Do something")
    }
}