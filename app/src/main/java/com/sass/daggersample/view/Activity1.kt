package com.sass.daggersample.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import com.sass.daggersample.R
import com.sass.daggersample.extra.AnotherThing
import com.sass.daggersample.viewmodel.SomeViewModel
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject
import javax.inject.Named

class Activity1 : DaggerAppCompatActivity() {

    @Inject
    lateinit var anotherThing: AnotherThing

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var someViewModel: SomeViewModel

    @Inject
    @Named("str2")
    lateinit var str2 : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.d("Value",anotherThing.message)

        someViewModel = ViewModelProvider(viewModelStore,viewModelFactory)
            .get(SomeViewModel::class.java)
        someViewModel.doSth()

        Log.d("Dagger2Sample",str2)

    }
}
