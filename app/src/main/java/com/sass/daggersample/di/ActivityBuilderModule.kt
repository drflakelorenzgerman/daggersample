package com.sass.daggersample.di

import com.sass.daggersample.di.viewmodel.ViewModelModule
import com.sass.daggersample.view.Activity1
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilderModule {
    @ContributesAndroidInjector(
        modules = [
            Activity1Module::class,
            FragmentsBuilderModule::class,
            ViewModelModule::class
        ]
    )
    abstract fun contributeActivity1(): Activity1
}