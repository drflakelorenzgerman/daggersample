package com.sass.daggersample.di

import com.sass.daggersample.view.Activity1
import com.sass.daggersample.view.Fragment1
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentsBuilderModule {
    @ContributesAndroidInjector()
    abstract fun contributeFragment1(): Fragment1

    // and other fragments
}