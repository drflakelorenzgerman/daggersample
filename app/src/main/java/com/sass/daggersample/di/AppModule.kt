package com.sass.daggersample.di

import com.sass.daggersample.extra.AnotherThing
import com.sass.daggersample.extra.Sth
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class AppModule {
    @Provides
    fun provideSth() : Sth{
        return Sth()
    }

    @Provides
    fun provideAnotherThing(sth: Sth) : AnotherThing{
        return AnotherThing(sth)
    }

    @Provides
    @Named("str1")
    fun provideRectangle(): String {
        return "STR 1"
    }
    @Provides
    @Named("str2")
    fun provideCircle(): String {
        return "STR 2"
    }
}