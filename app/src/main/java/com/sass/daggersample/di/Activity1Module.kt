package com.sass.daggersample.di

import dagger.Module

@Module
class Activity1Module {
    // you can provide some dependencies here , these dependencies are locally used in Activity1
    // so we don't need to provide them on AppModule
}