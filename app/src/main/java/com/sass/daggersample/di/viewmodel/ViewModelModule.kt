package com.sass.daggersample.di.viewmodel

import androidx.lifecycle.ViewModel
import com.sass.daggersample.viewmodel.SomeViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(SomeViewModel::class)
    abstract fun bindSplashViewModel(viewModel: SomeViewModel?): ViewModel?
}